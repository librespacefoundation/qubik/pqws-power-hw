# PQ9 Basic Power Module

This is a basic power module for PQ9ISH applications  

## Power input

Optimized for PV input, can also work with DC power source >5V  
Uses LT3652EDD for charging and MPPT  
Charge current 400mA can go up to 2A

## Power output:

Build around TPS63070RNM Buck/Boost DCDC converter

V1: 3.3V 1A  
V2: NC  
V3: NC  
V4: 5V 1A  

## Pinout

![](power.png)